script_game_start = 0
script_game_get_use_string = 1
script_game_quick_start = 2
script_spawn_quick_battle_army = 3
script_game_set_multiplayer_mission_end = 4
script_game_enable_cheat_menu = 5
script_game_troop_upgrades_button_clicked = 6
script_game_character_screen_requested = 7
script_game_get_console_command = 8
script_game_event_party_encounter = 9
script_game_event_simulate_battle = 10
script_game_event_battle_end = 11
script_game_get_item_buy_price_factor = 12
script_game_get_item_sell_price_factor = 13
script_game_event_buy_item = 14
script_game_event_sell_item = 15
script_game_get_troop_wage = 16
script_game_get_total_wage = 17
script_game_get_join_cost = 18
script_game_get_upgrade_xp = 19
script_game_get_upgrade_cost = 20
script_game_get_prisoner_price = 21
script_game_check_prisoner_can_be_sold = 22
script_game_get_morale_of_troops_from_faction = 23
script_game_event_detect_party = 24
script_game_event_undetect_party = 25
script_game_get_statistics_line = 26
script_game_get_date_text = 27
script_game_get_money_text = 28
script_game_get_party_companion_limit = 29
script_game_reset_player_party_name = 30
script_game_get_troop_note = 31
script_game_get_center_note = 32
script_game_get_faction_note = 33
script_game_get_quest_note = 34
script_game_get_info_page_note = 35
script_game_get_scene_name = 36
script_game_get_mission_template_name = 37
script_warn_player_about_auto_team_balance = 38
script_check_team_balance = 39
script_check_creating_ladder_dust_effect = 40
script_initialize_all_scene_prop_slots = 41
script_initialize_scene_prop_slots = 42
script_use_item = 43
script_determine_team_flags = 44
script_calculate_flag_move_time = 45
script_move_flag = 46
script_move_headquarters_flags = 47
script_set_num_agents_around_flag = 48
script_change_flag_owner = 49
script_move_object_to_nearest_entry_point = 50
script_multiplayer_server_on_agent_spawn_common = 51
script_multiplayer_server_player_joined_common = 52
script_multiplayer_server_before_mission_start_common = 53
script_multiplayer_client_on_agent_killed_or_wounded_common = 54
script_multiplayer_client_show_respawncounter = 55
script_multiplayer_server_on_agent_killed_or_wounded_common = 56
script_multiplayer_server_on_player_teamkilled_player = 57
script_multiplayer_close_gate_if_it_is_open = 58
script_multiplayer_move_moveable_objects_initial_positions = 59
script_team_set_score = 60
script_set_attached_scene_prop = 61
script_set_team_flag_situation = 62
script_initialize_objects = 63
script_initialize_objects_clients = 64
script_show_multiplayer_message = 65
script_show_multiplayer_message_custom_color = 66
script_get_headquarters_scores = 67
script_draw_this_round = 68
script_find_most_suitable_bot_to_control = 69
script_game_receive_url_response = 70
script_game_get_cheat_mode = 71
script_game_receive_network_message = 72
script_multiplayer_server_process_client_item_selection = 73
script_multiplayer_server_process_client_team_selection = 74
script_multiplayer_server_process_client_troop_selection = 75
script_multiplayer_server_play_hit_effect = 76
script_handle_cannon_hit_effect_event = 77
script_cf_multiplayer_evaluate_poll = 78
script_multiplayer_accept_duel = 79
script_game_get_multiplayer_server_option_for_mission_template = 80
script_game_multiplayer_server_option_for_mission_template_to_string = 81
script_cf_multiplayer_team_is_available = 82
script_game_multiplayer_event_duel_offered = 83
script_game_get_multiplayer_game_type_enum = 84
script_game_multiplayer_get_game_type_mission_template = 85
script_multiplayer_get_mission_template_game_type = 86
script_multiplayer_fill_available_factions_combo_button = 87
script_multiplayer_get_troop_class = 88
script_multiplayer_get_troop_rank = 89
script_cf_multiplayer_agent_is_musician = 90
script_multiplayer_clear_player_selected_items = 91
script_multiplayer_init_player_slots = 92
script_multiplayer_initialize_belfry_wheel_rotations = 93
script_send_open_close_information_of_object = 94
script_multiplayer_send_initial_information = 95
script_multiplayer_remove_headquarters_flags = 96
script_multiplayer_init_mission_variables = 97
script_multiplayer_set_item_available_for_troop = 98
script_multiplayer_send_item_selections = 99
script_multiplayer_fill_map_game_types = 100
script_multiplayer_count_players_bots = 101
script_multiplayer_find_player_leader_for_bot = 102
script_multiplayer_find_bot_troop_and_group_for_spawn = 103
script_multiplayer_change_leader_of_bot = 104
script_multiplayer_find_spawn_point = 105
script_multiplayer_find_spawn_point_2 = 106
script_multiplayer_buy_agent_equipment = 107
script_set_default_values_for_game_mode = 108
script_game_get_party_prisoner_limit = 109
script_game_get_item_extra_text = 110
script_game_on_disembark = 111
script_game_context_menu_get_buttons = 112
script_game_event_context_menu_button_clicked = 113
script_game_get_skill_modifier_for_troop = 114
script_game_check_party_sees_party = 115
script_game_get_party_speed_multiplier = 116
script_find_high_ground_around_pos1 = 117
script_select_battle_tactic = 118
script_select_battle_tactic_aux = 119
script_battle_tactic_init = 120
script_battle_tactic_init_aux = 121
script_battle_tactic_apply = 122
script_battle_tactic_apply_aux = 123
script_get_num_troops_in_division = 124
script_apply_effect_of_other_people_on_courage_scores = 125
script_apply_death_effect_on_courage_scores = 126
script_decide_run_away_or_not = 127
script_team_get_class_percentages = 128
script_get_closest3_distance_of_enemies_at_pos1 = 129
script_team_get_average_position_of_enemies = 130
script_init_town_walkers = 131
script_init_town_walker_agents = 132
script_tick_town_walkers = 133
script_set_town_walker_destination = 134
script_town_init_doors = 135
script_store_movement_order_name_to_s1 = 136
script_store_riding_order_name_to_s1 = 137
script_store_weapon_usage_order_name_to_s1 = 138
script_team_give_order_from_order_panel = 139
script_update_order_panel = 140
script_update_agent_position_on_map = 141
script_convert_3d_pos_to_map_pos = 142
script_update_order_flags_on_map = 143
script_update_order_panel_checked_classes = 144
script_update_order_panel_statistics_and_map = 145
script_agent_troop_get_banner_mesh_sp = 146
script_agent_troop_get_banner_mesh = 147
script_troop_agent_set_banner = 148
script_add_troop_to_cur_tableau = 149
script_add_troop_to_cur_tableau_for_character = 150
script_add_troop_to_cur_tableau_for_inventory = 151
script_add_troop_to_cur_tableau_for_profile = 152
script_add_troop_to_cur_tableau_for_party = 153
script_get_culture_with_party_faction_for_music = 154
script_get_culture_with_faction_for_music = 155
script_music_set_situation_with_culture = 156
script_combat_music_set_situation_with_culture = 157
script_custom_battle_end = 158
script_iterate_pointer_arrow = 159
script_multiplayer_broadcast_message = 160
script_store_bool_s9 = 161
script_multiplayer_server_update_custom_strings = 162
script_multiplayer_server_update_conquest_flag_strings = 163
script_multiplayer_server_slay_player = 164
script_multiplayer_server_revive_player = 165
script_multiplayer_server_god_mode = 166
script_multiplayer_server_freeze_player = 167
script_multiplayer_server_swap_player = 168
script_multiplayer_server_swap_teams = 169
script_multiplayer_server_spec_player = 170
script_multiplayer_server_play_sound_at_agent = 171
script_multiplayer_server_construct_prop = 172
script_multiplayer_server_initialise_destructable_prop_slots = 173
script_multiplayer_server_place_bomb = 174
script_multiplayer_server_hq_search_entrypoints_with_distance = 175
script_multiplayer_server_hq_get_entrypoints_for_flag = 176
script_multiplayer_generate_weather = 177
script_move_object_type_to_origional_position = 178
script_multiplayer_server_play_sound_at_position = 179
script_multiplayer_server_scale_prop_instance = 180
script_multiplayer_server_check_if_can_use_button = 181
script_find_or_create_scene_prop_instance = 182
script_clean_up_prop_instance = 183
script_clean_up_prop_instance_with_childs = 184
script_clean_up_prop_child_with_childs = 185
script_prop_instance_animate_to_position_with_childs = 186
script_prop_child_animate_to_position_with_childs = 187
script_prop_instance_find_first_child_of_type = 188
script_multiplayer_mm_refresh_artillery_availability = 189
script_multiplayer_mm_before_mission_start_common = 190
script_multiplayer_mm_reset_stuff_after_round_before_clear = 191
script_caesim_spawn_and_clear_molotovs = 192
script_multiplayer_mm_reset_stuff_after_round = 193
script_multiplayer_mm_refresh_inf_cav_availability = 194
script_multiplayer_mm_after_mission_start_common = 195
script_royale_initialise_cannons = 196
script_royale_initialise_boxes = 197
script_royale_spawn_box = 198
script_generate_bits_for_cannon_instance = 199
script_attach_limber_to_horse = 200
script_limber_cannon_to_horse = 201
script_copy_prop_slot = 202
script_unlimber_cannon_from_horse = 203
script_multiplayer_server_spawn_particle_at_position = 204
script_client_get_my_agent = 205
script_cannon_instance_get_wheels = 206
script_cannon_instance_get_barrel = 207
script_cannon_child_find_cannon_instance = 208
script_agent_take_cannonball = 209
script_push_flag_selection_to_player_if_needed = 210
script_cf_agent_is_playing_music = 211
script_cf_agent_is_playing_piano = 212
script_cf_agent_is_taking_a_shit = 213
script_cf_agent_is_surrendering = 214
script_get_playercounts_for_scoreboard = 215
script_get_playercounts_for_scoreboard_cb = 216
script_cf_common_kill_player_by_script = 217
script_random_item_selection = 218
script_quick_assign_item = 219
script_get_item_class_cur_item = 220
script_check_troop_availability = 221
script_get_num_players_of_troop = 222
script_get_num_players_of_troop_cb = 223
script_conquest_get_flag_name = 224
script_multiplayer_initalise_flags_common = 225
script_get_prop_kind_size_and_shift = 226
script_get_prop_center = 227
script_get_prop_scaled_size = 228
script_deliver_damage_to_prop = 229
script_get_destruction_properties_of_object = 230
script_get_next_stage_acording_to_damage = 231
script_get_next_destruction_stage_prop_kind = 232
script_get_default_health_for_prop_kind = 233
script_get_prop_instance_scale = 234
script_explosion_at_position = 235
script_caesim_hurt_at_position = 236
script_get_angle_of_ground_at_pos = 237
script_get_hightest_pos_and_angle_from_pos = 238
script_custom_battle_set_division_names = 239
script_custom_battle_deployment = 240
script_correct_num_troops_in_formation = 241
script_give_mm_order = 242
script_volley_fire = 243
script_deploy_division_via_teleport = 244
script_division_get_average_position = 245
script_spawn_crator_on_pos = 246
script_multiplayer_handle_prop_effect = 247
script_sp_common_before_mission_start = 248
script_sp_process_death_for_battle_results = 249
script_sp_camp_set_merchandise = 250
script_store_vince_random_in_range = 251
script_set_prop_child_inactive = 252
script_set_prop_child_active = 253
script_recoil_cannon = 254
script_lighting_strike = 255
script_commander_get_additional_bots = 256
script_scale_num_bots_after_troop_type = 257
script_on_commander_leave_or_team_switch = 258
script_return_team_with_least_players = 259
script_multiplayer_get_unit_type_for_select_presentation = 260
script_game_missile_launch = 261
script_game_missile_dives_into_water = 262
script_mm_on_bullet_hit = 263
script_set_agent_controlling_prop = 264
script_client_process_set_prop_control = 265
script_stop_agent_controlling_cannon = 266
script_fire_cannon = 267
script_multiplayer_server_agent_play_music = 268
script_multiplayer_server_agent_stop_music = 269
script_multiplayer_server_agent_use_spyglass = 270
script_multiplayer_agent_create_custom_order_menu = 271
script_multiplayer_agent_close_custom_order_menu = 272
script_multiplayer_agent_drinking_get_animation = 273
script_multiplayer_agent_drinking = 274
script_multiplayer_server_place_rocket = 275
script_multiplayer_server_agent_play_voicecommand = 276
script_reset_prop_slots = 277
script_attach_window_to_wall = 278
script_multiplayer_reset_round_time_if_no_agents = 279
script_get_prop_kind_for_constr_kind = 280
script_handle_agent_control_command = 281
script_cannon_explosion_on_position = 282
script_cannon_ball_hit_ground = 283
script_search_for_first_ground_from_direction_to_angle = 284
script_move_pioneer_ground = 285
script_multiplayer_server_stop_music_at_map_change = 286
script_multiplayer_server_disallow_multiple_firearms_on_pickup = 287
script_multiplayer_server_royale_use_weaponbox = 288
script_server_handle_bandages_hit = 289
script_server_send_on_agent_hit_event = 290
script_client_on_agent_hit = 291
script_multiplayer_server_send_player_score_kill_death = 292
script_multiplayer_client_apply_player_score_kill_death = 293
script_multiplayer_client_apply_prop_scale = 294
script_multiplayer_client_apply_prop_effect = 295
script_multiplayer_client_apply_destructible_prop_spawn_or_destroy = 296
script_multiplayer_client_play_sound_at_pos = 297
script_multiplayer_server_send_build_points = 298
script_multiplayer_server_protect_admin_password = 299
script_caesim_frag_explosion = 300
script_caesim_init_custom_usable_prop = 301
script_caesim_create_custom_usable_button = 302
script_agent_get_ammo_count = 303
script_caesim_get_map_string = 304
script_caesim_set_perms = 305
script_cf_caesim_player_is_super_admin = 306
script_cf_caesim_player_can_admin_player = 307
script_caesim_parse_command = 308
script_caesim_give_item = 309
script_caesim_init_auto_message = 310
script_send_colored_chat = 311
script_send_colored_chat_s0 = 312
script_send_colored_chat_range = 313
script_broadcast_admin_warning_s1 = 314
script_broadcast_admin_info_s1 = 315
script_broadcast_announcement_s1 = 316
script_show_player_misdeeds = 317
script_custom_command_share = 318
script_custom_command_admins = 319
script_custom_command_bps = 320
script_custom_command_players = 321
script_custom_command_prune = 322
script_custom_command_map = 323
script_log_punishment_s4 = 324
script_kick_player_and_log = 325
script_ban_player_and_log = 326
script_slay_player_and_log = 327
script_cf_troop_array_left_pop = 328
script_troop_array_left_shift = 329
script_troop_array_append = 330
script_choose_team_2_faction = 331
script_cf_array_contains = 332
script_switch_to_next_map = 333
script_add_map_to_queue = 334
script_add_map_to_history = 335
script_cf_caesim_map_has_not_been_used_recently = 336
script_caesim_map_get_full_name = 337
script_open_custom_menu = 338
script_process_custom_menu = 339
script_clear_owned_cannons = 340
script_cf_can_use_cannon = 341
script_wse_multiplayer_message_received = 342
script_wse_game_saved = 343
script_wse_savegame_loaded = 344
script_wse_chat_message_received = 345
script_wse_console_command_received = 346
script_wse_get_agent_scale = 347
script_wse_window_opened = 348
script_game_missile_dives_into_water = 349
script_wse_get_server_info = 350
script_caesim_get_map_settings = 351
script_get_scene_mission = 352
script_caesim_get_scene_alt_name = 353
script_cf_caesim_map_can_be_voted = 354
script_caesim_get_next_rotation_map = 355
script_cf_caesim_map_can_be_played = 356
script_cf_player_should_be_muted = 357
script_open_lootbox = 358
script_scene_id_string_to_int = 359


