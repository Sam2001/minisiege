## Notable commits:
### Bug fixes:
* 88b1cae1 - Fix of a fort part disappearing on destruction
* 70d7aa97 - Fix build points not being correctly given after destroying planks
* 1968fa31 - Fix getting stuck on cannon destruction when in use
* 9e8fc142 - Fix surrender reload exploit
### Fun stuff:
* e1789318 + 9bc916e9 - Cheer animation on B key
* 7f911435 - Usable props