#!/bin/bash
source config_shell.sh

echo Downloading configs...

sftp -P "$stage_port" "$stage_address" <<END

cd "$stage_path"

get config_*.py

END
