#!/bin/bash

IFS=$'\n'

echo `date`

while true; do
    urls=(`cat queries.txt`)
    truncate -s 0 queries.txt
    for (( i=0; i<${#urls[@]}; i++ )) ; do
        out="$(wget -qO- "${urls[$i]}")"
        echo "${urls[$i]} - $out"
    done
    sleep 1
done